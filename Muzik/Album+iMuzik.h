//
//  Album+iMuzik.h
//  iMuzik
//
//  Created by Bruno LAROSA on 2/16/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "Album.h"
#import "GDataXMLNode.h"

@interface Album (iMuzik)

+(Album *)albumWithMuzikInfo:(GDataXMLElement *)muzikInfo inManagedObjectContext:(NSManagedObjectContext *)context;

@end
