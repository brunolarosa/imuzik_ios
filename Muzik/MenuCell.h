//
//  MenuCell.h
//  iMuzik
//
//  Created by Bruno LAROSA on 2/14/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (strong, nonatomic) UILabel *menuLabel;
@property (strong, nonatomic) UIImageView *menuIcon;

@end
