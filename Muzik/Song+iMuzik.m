//
//  Song+iMuzik.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "Song+iMuzik.h"
#import "MuzikFetcher.h"
#import "Album+iMuzik.h"
#import "Artist+iMuzik.h"

@implementation Song (iMuzik)

+(Song *)songWithMuzikInfo:(GDataXMLElement *)muzikInfo inManagedObjectContext:(NSManagedObjectContext *)context {
    Song *song = nil;
    
    NSString *songId = [[[muzikInfo elementsForName:MUZIK_SONG_ID] objectAtIndex:0] stringValue];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Song"];
    request.predicate = [NSPredicate predicateWithFormat:@"unique = %@", songId];
    NSSortDescriptor *sortDescriptior = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptior];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if(!matches || matches.count > 1) {
        //TODO: Handle Errors
    } else if(matches.count == 0){
        song = [NSEntityDescription insertNewObjectForEntityForName:@"Song" inManagedObjectContext:context];
        song.unique = songId;
        song.title = [[[muzikInfo elementsForName:MUZIK_SONG_TITLE] objectAtIndex:0] stringValue];
        song.number = [[[muzikInfo elementsForName:MUZIK_SONG_NUMBER] objectAtIndex:0] stringValue];
        song.album = [Album albumWithMuzikInfo:[[muzikInfo elementsForName:MUZIK_ALBUM] objectAtIndex:0] inManagedObjectContext:context];
        song.artist = [Artist artistWithMuzikInfo:[[muzikInfo elementsForName:MUZIK_ARTIST] objectAtIndex:0] inManagedObjectContext:context];
        song.album.artist = song.artist;
        
    } else {
        song = [matches lastObject];
    }
    
    return song;
}

@end
