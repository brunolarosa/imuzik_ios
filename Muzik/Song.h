//
//  Song.h
//  Muzik
//
//  Created by Bruno LAROSA on 2/18/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Album.h"
#import "Artist.h"

@class PlaylistItem;

@interface Song : NSManagedObject

@property (nonatomic, retain) NSString * number;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * unique;
@property (nonatomic, retain) Album *album;
@property (nonatomic, retain) Artist *artist;
@property (nonatomic, retain) NSSet *playlistItems;
@end

@interface Song (CoreDataGeneratedAccessors)

- (void)addPlaylistItemsObject:(PlaylistItem *)value;
- (void)removePlaylistItemsObject:(PlaylistItem *)value;
- (void)addPlaylistItems:(NSSet *)values;
- (void)removePlaylistItems:(NSSet *)values;

@end
