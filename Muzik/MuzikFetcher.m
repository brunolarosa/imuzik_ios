//
//  MuzikFetcher.m
//  iMuzik
//
//  Created by Bruno LAROSA on 2/16/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "MuzikFetcher.h"



@implementation MuzikFetcher

+ (GDataXMLDocument *)executeiMuzikFetch:(NSString *)query
{
    query = [NSString stringWithFormat:@"%@", query];
    query = [query stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // NSLog(@"[%@ %@] sent %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), query);
    NSData *xmlData = [[NSString stringWithContentsOfURL:[NSURL URLWithString:query] encoding:NSUTF8StringEncoding error:nil] dataUsingEncoding:NSUTF8StringEncoding];
    

    NSError *error = nil;
    GDataXMLDocument *results = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    
    return results;
}


+ (NSArray *)getAllArtists {
    NSString *request = [NSString stringWithFormat:@"%@/resources/artists/all", SERVER];
    GDataXMLDocument *doc = [self executeiMuzikFetch:request];
    return [doc.rootElement elementsForName:MUZIK_ARTIST];
}

+ (NSArray *)getAllSongs {
    NSString *request = [NSString stringWithFormat:@"%@/resources/songs/all", SERVER];
    GDataXMLDocument *doc = [self executeiMuzikFetch:request];
    return [doc.rootElement elementsForName:MUZIK_SONG];
}

+ (NSArray *)getAllPlaylists {
    NSString *request = [NSString stringWithFormat:@"%@/resources/playlists/all", SERVER];
    GDataXMLDocument *doc = [self executeiMuzikFetch:request];
    return [doc.rootElement elementsForName:MUZIK_PLAYLIST];

}

@end
