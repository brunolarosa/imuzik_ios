//
//  ArtistViewController.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "ArtistViewController.h"
#import "Artist.h"
#import "Album.h"

#import "FRLayeredNavigation.h"
#import "AlbumViewController.h"

//VIEW IMPORT
#import "AlbumCollectionViewCell.h"

#define COLLECTIONVIEW_BG [UIColor colorWithRed:212.0/255.0 green:212.0/255.0 blue:212.0/255.0 alpha:1.0]

@interface ArtistViewController ()

@property (strong, nonatomic) NSArray *albums;

@end

@implementation ArtistViewController

@synthesize artist = _artist;
@synthesize albums = _albums;

- (NSArray *)albums {
    if (!_albums) {
        _albums = [[self.artist.albums allObjects] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES]]];
    }
    
    return _albums;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)customizeUI {
    self.view.backgroundColor = BG_COLOR;
    self.albumsCollectionView.backgroundColor = COLLECTIONVIEW_BG;
}

#pragma mark - view cycle life
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.artistNameLabel setText:[self.artist name]];
    [self.artistImageView setImage:[UIImage imageWithData:self.artist.image]];
    [self.artistBioLabel setText:[self.artist bio]];
    
    [self.topTrackTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Track"];
    [self.albumsCollectionView registerClass:[AlbumCollectionViewCell class] forCellWithReuseIdentifier:@"Album"];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - tableview datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Track";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.albums count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    AlbumCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"Album" forIndexPath:indexPath];
    Album *currentAlbum = [self.albums objectAtIndex:indexPath.row];
    
    [cell.albumTitleLabel setText:currentAlbum.title];
    [cell.albumCoverImageView setImage:[UIImage imageWithData:currentAlbum.coverData]];
    return cell;
}

- (UICollectionReusableView *)collectionView:
(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    AlbumViewController *albumController = [[AlbumViewController alloc] initWithNibName:@"AlbumViewController" bundle:nil];
    [albumController setAlbum:[self.albums objectAtIndex:indexPath.row]];
    [self.layeredNavigationController pushViewController:albumController inFrontOf:self maximumWidth:NO animated:YES configuration:^(FRLayeredNavigationItem *item) {
        item.nextItemDistance = 400;
        item.hasChrome = NO;
        item.width = 472;
        
    }];
    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark – UICollectionViewDelegateFlowLayout

 - (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
 return CGSizeMake(85, 100);
 }
 
/* // 3
 
 - (UIEdgeInsets)collectionView:
 (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
 if ([[self.comparisonCriteriaVM.sortedBrands objectAtIndex:section] count] > 0) {
 return UIEdgeInsetsMake(50, 20, 50, 20);
 } else {
 return UIEdgeInsetsZero;
 }
 
 }
 */




@end
