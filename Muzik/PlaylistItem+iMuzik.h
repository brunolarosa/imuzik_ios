//
//  PlaylistItem+iMuzik.h
//  Muzik
//
//  Created by Bruno LAROSA on 2/18/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "PlaylistItem.h"
#import "GDataXMLNode.h"

@interface PlaylistItem (iMuzik)

+(PlaylistItem *)playlistItemWithMuzikInfo:(GDataXMLElement *)playlistItemElement inManagedObjectContext:(NSManagedObjectContext *)context;

@end
