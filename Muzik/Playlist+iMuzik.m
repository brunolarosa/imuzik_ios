//
//  Playlist+iMuzik.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/18/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "Playlist+iMuzik.h"
#import "MuzikFetcher.h"
#import "PlaylistItem+iMuzik.h"

@implementation Playlist (iMuzik)

+(Playlist *)playlistWithMuzikInfo:(GDataXMLElement *)playlistElement inManagedObjectContext:(NSManagedObjectContext *)context {
    Playlist *playlist = nil;
    
    NSString *playlistId = [[[playlistElement elementsForName:MUZIK_PLAYLIST_ID] objectAtIndex:0] stringValue];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Playlist"];
    request.predicate = [NSPredicate predicateWithFormat:@"unique = %@", playlistId];
    NSSortDescriptor *sortDescriptior = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptior];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if(!matches || matches.count > 1) {
        //TODO: Handle Errors
    } else if(matches.count == 0){
        playlist = [NSEntityDescription insertNewObjectForEntityForName:@"Playlist" inManagedObjectContext:context];
        playlist.unique = playlistId;
        playlist.name = [[[playlistElement elementsForName:MUZIK_PLAYLIST_NAME] objectAtIndex:0] stringValue];
        
        NSArray *items = [[[playlistElement elementsForName:MUZIK_PLAYLIST_ITEMS] objectAtIndex:0] elementsForName:MUZIK_PLAYLIST_ITEM];
        for (GDataXMLElement *itemElement in items) {
            [playlist addItemsObject:[PlaylistItem playlistItemWithMuzikInfo:itemElement inManagedObjectContext:context]];
        }
        
    } else {
        playlist = [matches lastObject];
    }
    
    return playlist;
}

@end
