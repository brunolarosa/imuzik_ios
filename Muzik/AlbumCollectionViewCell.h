//
//  AlbumCollectionViewCell.h
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) UILabel *albumTitleLabel;
@property (strong, nonatomic) UIImageView *albumCoverImageView;

@end
