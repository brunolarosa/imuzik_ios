//
//  ArtistsListViewController.h
//  Muzik
//
//  Created by Bruno LAROSA on 2/18/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistsListViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (nonatomic,strong) UIManagedDocument* muzikDatabase;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end
