//
//  Song+iMuzik.h
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "Song.h"
#import "GDataXMLNode.h"

@interface Song (iMuzik)

+(Song *)songWithMuzikInfo:(GDataXMLElement *)muzikInfo inManagedObjectContext:(NSManagedObjectContext *)context;

@end
