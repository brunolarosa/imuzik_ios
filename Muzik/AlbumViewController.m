//
//  AlbumViewController.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "AlbumViewController.h"
#import "AppDelegate.h"
#import "Album.h"
#import "Song.h"
#import "MuzikPlayerItem.h"

@interface AlbumViewController ()

@property (strong, nonatomic) NSArray *tracks;

@end

@implementation AlbumViewController

@synthesize album = _album;
@synthesize tracks = _tracks;

- (NSArray *)tracks {
    if (!_tracks) {
        _tracks = [[self.album.songs allObjects] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES]]];
    }
    return _tracks;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:BG_COLOR];
    [self.albumCoverImageView setImage:[UIImage imageWithData:self.album.coverData]];
    [self.tracksTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Track"];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tracks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Track";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Song *song = [self.tracks objectAtIndex:indexPath.row];
    
    [cell.textLabel setText:song.title];
    // Configure the cell...
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */ 
#pragma mark - tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Song *song = [self.tracks objectAtIndex:indexPath.row];
    AppDelegate *delegate  = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    delegate.playerSongs = [self createAlbumPlaylist];
    //delegate.player = [AVQueuePlayer queuePlayerWithItems:[self createAlbumPlaylist]];
    [delegate playAtIndex:indexPath.row];
    //[delegate.player play];
    /*NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.19.102:8080/iMuzik-war/musicServlet?m=%@",song.unique]];
    AVURLAsset *asset = [AVURLAsset assetWithURL:url];
    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:<#(AVAsset *)#>];
    item tr
    delegate.player = [AVPlayer playerWithURL:url];
    [delegate.player play];
    delegate.player i*/
    
}

- (NSMutableArray *)createAlbumPlaylist {
    NSMutableArray *playlist = [NSMutableArray arrayWithCapacity:self.tracks.count];
    
    for (Song *track in self.tracks) {
        [playlist addObject:track];
    }
    
    return playlist;
}

@end
