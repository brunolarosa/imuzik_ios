//
//  PlayerViewController.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "PlayerViewController.h"
#import "AppDelegate.h"
#import "MuzikPlayerItem.h"
#import "Song.h"

@interface PlayerViewController ()

@end

@implementation PlayerViewController

- (AVQueuePlayer *)player {
    return [((AppDelegate *)[[UIApplication sharedApplication]delegate]) player];
}

- (AppDelegate *)appDelegate {
    return (AppDelegate *)[[UIApplication sharedApplication]delegate];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self syncPlayPauseButtons];
    
    [self syncSong];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)syncSong {
    MuzikPlayerItem *item = (MuzikPlayerItem *)[[self player] currentItem];
    
    [self.trackTitlelabel setText:item.song.title];
    [self.artistNameLabel setText:item.song.album.artist.name];
    [self.albumCoverImageView setImage:[UIImage imageWithData:item.song.album.coverData]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)previousButtonPressed:(UIButton *)sender {
    [[self appDelegate] playPreviousForCurrentItem:(MuzikPlayerItem *)[[self player] currentItem]];
    [[self player] play];
}

- (IBAction)playButtonPressed:(UIButton *)sender {
    [[self player] play];
    [self showPauseButton];
}

- (IBAction)nextButtonPressed:(UIButton *)sender {
    [[self player] advanceToNextItem];
    [self syncSong];
}

- (IBAction)pauseButtonPressed:(UIButton *)sender {
    [[self player] pause];
    [self showPlayButton];
    
}

- (void)showPlayButton {
    [self.playButton setHidden:NO];
    [self.pauseButton setHidden:YES];
}

- (void)showPauseButton {
    [self.playButton setHidden:YES];
    [self.pauseButton setHidden:NO];
}



/* ---------------------------------------------------------
**  Methods to handle manipulation of the movie scrubber control
** ------------------------------------------------------- */

/* Requests invocation of a given block during media playback to update the movie scrubber control. */
-(void)initScrubberTimer
{
	double interval = .1f;
	
	CMTime playerDuration = [self playerItemDuration];
	if (CMTIME_IS_INVALID(playerDuration))
	{
		return;
	}
	double duration = CMTimeGetSeconds(playerDuration);
	if (isfinite(duration))
	{
		CGFloat width = CGRectGetWidth([self.scrubberSlider bounds]);
		interval = 0.5f * duration / width;
	}
    
	/* Update the scrubber during normal playback. */
	mTimeObserver = [[self player] addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(interval, NSEC_PER_SEC)
                                                           queue:NULL /* If you pass NULL, the main queue is used. */
                                                      usingBlock:^(CMTime time)
                      {
                          [self syncScrubber];
                      }];
    
}

/* Set the scrubber based on the player current time. */
- (void)syncScrubber
{
	CMTime playerDuration = [self playerItemDuration];
	if (CMTIME_IS_INVALID(playerDuration))
	{
		self.scrubberSlider.minimumValue = 0.0;
		return;
	}
    
	double duration = CMTimeGetSeconds(playerDuration);
	if (isfinite(duration))
	{
		float minValue = [self.scrubberSlider minimumValue];
		float maxValue = [self.scrubberSlider maximumValue];
		double time = CMTimeGetSeconds([[self player] currentTime]);
		
		[self.scrubberSlider setValue:(maxValue - minValue) * time / duration + minValue];
	}
}


/* ---------------------------------------------------------
 **  Get the duration for a AVPlayerItem.
 ** ------------------------------------------------------- */

- (CMTime)playerItemDuration
{
	AVPlayerItem *playerItem = [[self player] currentItem];
	if (playerItem.status == AVPlayerItemStatusReadyToPlay)
	{
        /*
         NOTE:
         Because of the dynamic nature of HTTP Live Streaming Media, the best practice
         for obtaining the duration of an AVPlayerItem object has changed in iOS 4.3.
         Prior to iOS 4.3, you would obtain the duration of a player item by fetching
         the value of the duration property of its associated AVAsset object. However,
         note that for HTTP Live Streaming Media the duration of a player item during
         any particular playback session may differ from the duration of its asset. For
         this reason a new key-value observable duration property has been defined on
         AVPlayerItem.
         
         See the AV Foundation Release Notes for iOS 4.3 for more information.
         */
        
		return([playerItem duration]);
	}
	
	return(kCMTimeInvalid);
}

- (BOOL)isPlaying
{
	return [[self player] rate] != 0.f;
}

- (void)syncPlayPauseButtons {
    if ([self isPlaying])
	{
        [self showPauseButton];
	}
	else
	{
        [self showPlayButton];
	}
}


@end
