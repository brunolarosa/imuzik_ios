//
//  AlbumCollectionViewCell.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "AlbumCollectionViewCell.h"

@implementation AlbumCollectionViewCell

@synthesize albumCoverImageView = _albumCoverImageView;
@synthesize albumTitleLabel = _albumTitleLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        
        self.albumTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height-20, frame.size.width, 20)];
        [self.albumTitleLabel setFont:[UIFont systemFontOfSize:12]];
        [self.albumTitleLabel setTextAlignment:NSTextAlignmentCenter];
        self.albumTitleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.albumTitleLabel];
        
        self.albumCoverImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, frame.size.width-10, frame.size.height-25)];
        [self addSubview:self.albumCoverImageView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
