//
//  PlaylistItem.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/18/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "PlaylistItem.h"



@implementation PlaylistItem

@dynamic position;
@dynamic unique;
@dynamic playlist;
@dynamic song;

@end
