//
//  PlaylistItem+iMuzik.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/18/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "PlaylistItem+iMuzik.h"
#import "MuzikFetcher.h"

#import "Song+iMuzik.h"

@implementation PlaylistItem (iMuzik)

+(PlaylistItem *)playlistItemWithMuzikInfo:(GDataXMLElement *)playlistItemElement inManagedObjectContext:(NSManagedObjectContext *)context {
    PlaylistItem *item = nil;
    
    NSString *itemId = [[[playlistItemElement elementsForName:MUZIK_PLAYLIST_ITEM_ID] objectAtIndex:0] stringValue];
    
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"PlaylistItem"];
    request.predicate = [NSPredicate predicateWithFormat:@"unique = %@", itemId];
    NSSortDescriptor *sortDescriptior = [NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptior];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if(!matches || matches.count > 1) {
        //TODO: Handle Errors
    } else if(matches.count == 0){
        item = [NSEntityDescription insertNewObjectForEntityForName:@"PlaylistItem" inManagedObjectContext:context];
        item.unique = itemId;
        item.position = [[[playlistItemElement elementsForName:MUZIK_PLAYLIST_ITEM_POSITION] objectAtIndex:0] stringValue];
        item.song = [Song songWithMuzikInfo:[[playlistItemElement elementsForName:MUZIK_SONG] objectAtIndex:0] inManagedObjectContext:context];
        
    } else {
        item = [matches lastObject];
    }
    
    return item;
}

@end
