//
//  MuzikPlayerItem.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "MuzikPlayerItem.h"
#import "Song.h"

@implementation MuzikPlayerItem

@synthesize song = _song;

- (id)initWithSong:(Song *)song {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/musicServlet?m=%@", SERVER,song.unique]];
    self = [super initWithURL:url];
    if (self) {
        self.song = song;
    }
    return self;
}


+ (MuzikPlayerItem *)playerItemWithSong:(Song *)song {
    MuzikPlayerItem *item = [[MuzikPlayerItem alloc] initWithSong:song];
    return item;
}

@end
