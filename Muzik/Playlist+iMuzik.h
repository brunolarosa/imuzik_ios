//
//  Playlist+iMuzik.h
//  Muzik
//
//  Created by Bruno LAROSA on 2/18/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "Playlist.h"
#import "GDataXMLNode.h"

@interface Playlist (iMuzik)

+(Playlist *)playlistWithMuzikInfo:(GDataXMLElement *)playlistElement inManagedObjectContext:(NSManagedObjectContext *)context;

@end
