//
//  MuzikPlayerItem.h
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
@class Song;

@interface MuzikPlayerItem : AVPlayerItem

@property (strong, nonatomic) Song *song;

- (id)initWithSong:(Song *)song;
+ (MuzikPlayerItem *)playerItemWithSong:(Song *)song;

@end
