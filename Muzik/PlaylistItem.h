//
//  PlaylistItem.h
//  Muzik
//
//  Created by Bruno LAROSA on 2/18/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "Playlist.h"
#import "Song.h"

@interface PlaylistItem : NSManagedObject

@property (nonatomic, retain) NSString * position;
@property (nonatomic, retain) NSString * unique;
@property (nonatomic, retain) Playlist *playlist;
@property (nonatomic, retain) Song *song;

@end
