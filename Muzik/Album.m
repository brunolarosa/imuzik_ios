//
//  Album.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "Album.h"
#import "Artist.h"
#import "Song.h"


@implementation Album

@dynamic title;
@dynamic unique;
@dynamic imageUrl;
@dynamic coverData;
@dynamic artist;
@dynamic songs;

@end
