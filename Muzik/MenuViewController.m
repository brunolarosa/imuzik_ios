//
//  MenuViewController.m
//  iMuzik
//
//  Created by Bruno LAROSA on 2/12/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "MenuViewController.h"
#import "FRLayeredNavigation.h"

// IMPORT VIEW
#import "MenuCell.h"

#import "PlaylistsViewController.h"
#import "PlayerViewController.h"
#import "ArtistsListViewController.h"

// DEFINE
#define CELL_IMAGE @"cellImage"
#define CELL_IMAGE_SELECTED @"cellImageSelected"
#define CELL_TEXT @"cellText"



@interface MenuViewController ()

@property (strong, nonatomic) NSArray *menuCellContent;

@end

@implementation MenuViewController

@synthesize menuCellContent = _menuCellContent;

#pragma mark - GETTERS AND SETTERS
- (NSArray *)menuCellContent {
    if (!_menuCellContent) {
        NSMutableArray *temp = [[NSMutableArray alloc] initWithCapacity:0];
        [temp addObject:[NSDictionary dictionaryWithObjectsAndKeys:[UIImage imageNamed:@"menuListen_inverse.png"], CELL_IMAGE, @"A l'écoute", CELL_TEXT, nil]];
        
        [temp addObject:[NSDictionary dictionaryWithObjectsAndKeys:[UIImage imageNamed:@"menuPlaylists_inverse.png"], CELL_IMAGE, @"Playlists", CELL_TEXT, [UIImage imageNamed:@"menuPlaylists_selected.png"], CELL_IMAGE_SELECTED, nil]];
        [temp addObject:[NSDictionary dictionaryWithObjectsAndKeys:[UIImage imageNamed:@"menuArtist_inverse.png"], CELL_IMAGE, @"Artists", CELL_TEXT, nil]];
        
        _menuCellContent = temp;
    }
    return _menuCellContent;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.tableView registerClass:[MenuCell class] forCellReuseIdentifier:@"Cell"];
    [self customTableView];
    
    AVPlayer *item = [AVPlayer playerWithURL:[NSURL URLWithString:@"http://192.168.19.102:8080/iMuzik-war/musicServlet?m=2"]];
    [item play];
    
    

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom UI
- (void)customTableView {
    [self.view setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setRowHeight:80];
    [self.tableView setScrollEnabled:NO];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.menuCellContent count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.menuLabel.text = [[self.menuCellContent objectAtIndex:indexPath.row] objectForKey:CELL_TEXT];
    [cell.menuIcon setImage:[[self.menuCellContent objectAtIndex:indexPath.row] objectForKey:CELL_IMAGE]];
    
    // Configure the cell...
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0: {
            PlayerViewController *playerController = [[PlayerViewController alloc] init];
            [self.layeredNavigationController pushViewController:playerController inFrontOf:self maximumWidth:YES
                                                        animated:NO configuration:^(FRLayeredNavigationItem *item) {
                item.nextItemDistance = 0;
                item.hasChrome = NO;
                item.width = 472;
            }];
            
            break;
        }
        case 1: {
            PlaylistsViewController *playlistsController = [[PlaylistsViewController alloc] init];
            
            [self.layeredNavigationController pushViewController:playlistsController inFrontOf:self maximumWidth:NO animated:NO configuration:^(FRLayeredNavigationItem *item) {
                item.nextItemDistance = 0;
                item.hasChrome = NO;
                item.width = 472;
                    }];
            break;
        }
        case 2: {
            ArtistsListViewController *artistsController = [[ArtistsListViewController alloc] init];
            
            [self.layeredNavigationController pushViewController:artistsController inFrontOf:self maximumWidth:NO animated:NO configuration:^(FRLayeredNavigationItem *item) {
                item.nextItemDistance = 0;
                item.hasChrome = NO;
                item.width = 472;
            }];
            break;
        }
        default:
            break;
    }
    
        

    
}

@end
