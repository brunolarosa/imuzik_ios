//
//  PlayerViewController.h
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerViewController : UIViewController  {
    id mTimeObserver;
}

@property (weak, nonatomic) IBOutlet UIImageView *albumCoverImageView;
@property (weak, nonatomic) IBOutlet UILabel *trackTitlelabel;
@property (weak, nonatomic) IBOutlet UILabel *artistNameLabel;
@property (weak, nonatomic) IBOutlet UISlider *scrubberSlider;
@property (weak, nonatomic) IBOutlet UIButton *pauseButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UILabel *currentTimeLabel;
@property (strong, nonatomic) IBOutlet UIView *durationLabel;

- (IBAction)previousButtonPressed:(UIButton *)sender;
- (IBAction)playButtonPressed:(UIButton *)sender;
- (IBAction)nextButtonPressed:(UIButton *)sender;
- (IBAction)pauseButtonPressed:(UIButton *)sender;




@end
