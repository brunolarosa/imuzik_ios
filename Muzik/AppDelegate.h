//
//  AppDelegate.h
//  Muzik
//
//  Created by Bruno LAROSA on 2/16/13.
//  Copyright (c) 2013 Bruno LARO/Users/brunolarosa/Developer/iOS/Muzik/Muzik/MuzikPlayerItem.hSA. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MuzikPlayerItem;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIManagedDocument *muzikDatabase;
@property (strong, nonatomic) AVQueuePlayer *player;
@property (strong, nonatomic) NSMutableArray *playerSongs;

- (void)playAtIndex:(NSInteger)index;
- (void)playPreviousForCurrentItem:(MuzikPlayerItem *)item;

@end
