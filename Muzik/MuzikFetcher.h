//
//  MuzikFetcher.h
//  iMuzik
//
//  Created by Bruno LAROSA on 2/16/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GDataXMLNode.h"

#define MUZIK_ARTISTS @"artists"
#define MUZIK_ARTIST @"artist"
#define MUZIK_ARTIST_ID @"id"
#define MUZIK_ARTIST_NAME @"name"
#define MUZIK_ARTIST_BIO @"bio"
#define MUZIK_ARTIST_IMAGE_URL @"imageURL"
#define MUZIK_ARTIST_ALBUMS @"albums"

#define MUZIK_ALBUMS @"albums"
#define MUZIK_ALBUM @"album"
#define MUZIK_ALBUM_ID @"id"
#define MUZIK_ALBUM_TITLE @"title"
#define MUZIK_ALBUM_IMAGE_URL @"imageURL"

#define MUZIK_SONG @"song"
#define MUZIK_SONGS @"songs"
#define MUZIK_SONG_ID @"id"
#define MUZIK_SONG_TITLE @"title"
#define MUZIK_SONG_NUMBER @"number"


#define MUZIK_PLAYLIST @"playlist"
#define MUZIK_PLAYLIST_ID @"id"
#define MUZIK_PLAYLIST_NAME @"name"
#define MUZIK_PLAYLIST_ITEMS @"items"
#define MUZIK_PLAYLIST_ITEM @"item"
#define MUZIK_PLAYLIST_ITEM_ID @"id"
#define MUZIK_PLAYLIST_ITEM_POSITION @"pos"

@interface MuzikFetcher : NSObject

+ (NSArray *)getAllArtists;
+ (NSArray *)getAllPlaylists;
+ (NSArray *)getAllSongs;

@end
