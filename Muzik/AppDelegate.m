//
//  AppDelegate.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/16/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "AppDelegate.h"
#import "MuzikFetcher.h"
#import "MuzikPlayerItem.h"
#import "Song+iMuzik.h"
#import "PlaylistsViewController.h"
#import "MenuViewController.h"
#import "FRLayeredNavigation.h"

@implementation AppDelegate

@synthesize muzikDatabase = _muzikDatabase;
@synthesize player = _player;
@synthesize playerSongs = _playerSongs;

-(void)setMuzikDatabase:(UIManagedDocument *)muzikDatabase {
    _muzikDatabase = muzikDatabase;
    [self useDocument];
}
- (void)useDocument {
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self.muzikDatabase.fileURL path]]) {
        
        [self.muzikDatabase saveToURL:self.muzikDatabase.fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success){
            if (success) {
                //[self fetchMuzikDataIntoDocument:self.muzikDatabase];
            } else {
                NSLog(@"Error can't create document !");
            }
            
        }];
    } else if (self.muzikDatabase.documentState == UIDocumentStateClosed) {
        
        [self.muzikDatabase openWithCompletionHandler:^(BOOL success){
            if (success) {
                NSLog(@"Open");
                //[self fetchMuzikDataIntoDocument:self.muzikDatabase];
                
            } else {
                NSLog(@"FAIL OPEN !");
            }
        }];
        
    } else if(self.muzikDatabase.documentState == UIDocumentStateNormal) {
        
    }
}


#pragma mark - public methods
- (NSInteger)indexForCurrentItem:(MuzikPlayerItem *)item {
    for (int i = 0 ; i < self.playerSongs.count; i++) {
        if (item.song == [self.playerSongs objectAtIndex:i]) {
            return [[NSNumber numberWithInt:i] integerValue];
        }
    }
    return 0;
}

- (void)playPreviousForCurrentItem:(MuzikPlayerItem *)item {
    int index = [self indexForCurrentItem:item];
    if (0 < index) {
        index --;
    }
    [self playAtIndex:index];
}

- (void)playAtIndex:(NSInteger)index
{
    [self.player removeAllItems];
    self.player = [AVQueuePlayer queuePlayerWithItems:[self playlistWithStartIndex:index]];
    [self.player play];
}

- (NSArray *)playlistWithStartIndex:(NSInteger)index {
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = index; i < self.playerSongs.count; i++) {
        MuzikPlayerItem * obj = [MuzikPlayerItem playerItemWithSong:[self.playerSongs objectAtIndex:i]];
        [array addObject:obj];
    }
    return array;
}
/*- (void)fetchMuzikDataIntoDocument: (UIManagedDocument *)document
{
    dispatch_queue_t fetchQ = dispatch_queue_create("MuzikFetcher", NULL);
    dispatch_async(fetchQ, ^{
       
        [document.managedObjectContext performBlock:^{
            NSArray *songs = [MuzikFetcher getAllSongs];
            for (GDataXMLElement *songElement in songs) {
                [Song songWithMuzikInfo:songElement inManagedObjectContext:document.managedObjectContext];
            }
        }];
        
        
    });
}*/

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if(!self.muzikDatabase) {
        NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        url = [url URLByAppendingPathComponent:@"MuzikDatabase"];
        self. muzikDatabase = [[UIManagedDocument alloc] initWithFileURL:url];
    }
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
    
    MenuViewController *menuController = [[MenuViewController alloc] init];
    
    FRLayeredNavigationController *lnc = [[FRLayeredNavigationController alloc] initWithRootViewController:menuController configuration:^(FRLayeredNavigationItem *item) {
        item.nextItemDistance = 80;
        item.hasChrome = NO;
        item.width = 80;
    }];

    self.window.rootViewController = lnc;    
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
