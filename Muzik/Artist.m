//
//  Artist.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "Artist.h"
#import "Album.h"
#import "Song.h"


@implementation Artist

@dynamic bio;
@dynamic name;
@dynamic unique;
@dynamic imageUrl;
@dynamic image;
@dynamic albums;
@dynamic tracks;

@end
