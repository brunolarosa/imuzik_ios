//
//  PlaylistViewController.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "PlaylistViewController.h"
#import "Playlist.h"
#import "PlaylistItem.h"
#import "AppDelegate.h"
#import "MuzikPlayerItem.h"

@interface PlaylistViewController ()

@property (strong, nonatomic) NSArray *tracks;

@end

@implementation PlaylistViewController

@synthesize playlist = _playlist;
@synthesize tracks = _tracks;

- (NSArray *)tracks {
    if (!_tracks) {
        NSLog(@"Items : %i", [self.playlist.items count]);
        _tracks = [[self.playlist.items allObjects] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES]]];
    }
    return _tracks;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:BG_COLOR];
    [self.tracksTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Track"];
    [self.playlistNameLabel setText:self.playlist.name];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tracks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Track";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    PlaylistItem *item = [self.tracks objectAtIndex:indexPath.row];
    
    [cell.textLabel setText:item.song.title];
    // Configure the cell...
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */
#pragma mark - tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Song *song = [self.tracks objectAtIndex:indexPath.row];
    AppDelegate *delegate  = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    
    delegate.playerSongs = [self createAlbumPlaylist];
    //delegate.player = [AVQueuePlayer queuePlayerWithItems:[self createAlbumPlaylist]];
    [delegate playAtIndex:indexPath.row];
    /*NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.19.102:8080/iMuzik-war/musicServlet?m=%@",song.unique]];
     AVURLAsset *asset = [AVURLAsset assetWithURL:url];
     AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:<#(AVAsset *)#>];
     item tr
     delegate.player = [AVPlayer playerWithURL:url];
     [delegate.player play];
     delegate.player i*/
    
}

- (NSMutableArray *)createAlbumPlaylist {
    NSMutableArray *playlist = [NSMutableArray arrayWithCapacity:self.tracks.count];
    
    for (PlaylistItem *track in self.tracks) {
        [playlist addObject:track.song];
    }
    
    return playlist;
}


@end
