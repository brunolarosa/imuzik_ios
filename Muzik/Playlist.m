//
//  Playlist.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/18/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "Playlist.h"
#import "PlaylistItem.h"


@implementation Playlist

@dynamic name;
@dynamic unique;
@dynamic items;

@end
