//
//  Artist+iMuzik.m
//  iMuzik
//
//  Created by Bruno LAROSA on 2/16/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "Artist+iMuzik.h"

#import "Album+iMuzik.h"
#import "MuzikFetcher.h"

@implementation Artist (iMuzik)

+(Artist *)artistWithMuzikInfo:(GDataXMLElement *)muzikInfo inManagedObjectContext:(NSManagedObjectContext *)context {
    
    Artist *artist = nil;
    NSString *artistId = [[[muzikInfo elementsForName:MUZIK_ARTIST_ID] objectAtIndex:0]stringValue];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Artist"];
    request.predicate = [NSPredicate predicateWithFormat:@"unique = %@", artistId];
    
    NSSortDescriptor *sortDescriptior = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptior];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if(!matches || matches.count > 1) {
        //TODO: Handle Errors
    } else if(matches.count == 0){
        
        artist = [NSEntityDescription insertNewObjectForEntityForName:@"Artist" inManagedObjectContext:context];
        artist.unique = [[[muzikInfo elementsForName:MUZIK_ARTIST_ID] objectAtIndex:0] stringValue];
        artist.name = [[[muzikInfo elementsForName:MUZIK_ARTIST_NAME] objectAtIndex:0] stringValue];
        artist.bio = [[[muzikInfo elementsForName:MUZIK_ARTIST_BIO] objectAtIndex:0] stringValue];
        artist.imageUrl = [[[muzikInfo elementsForName:MUZIK_ARTIST_IMAGE_URL] objectAtIndex:0] stringValue];
        artist.image = [NSData dataWithContentsOfURL:[NSURL URLWithString:artist.imageUrl]];
        
    } else {
        artist = [matches lastObject];
    }
    
    return artist;
}

@end
