//
//  Song.m
//  Muzik
//
//  Created by Bruno LAROSA on 2/18/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "Song.h"
#import "PlaylistItem.h"


@implementation Song

@dynamic number;
@dynamic title;
@dynamic unique;
@dynamic album;
@dynamic artist;
@dynamic playlistItems;

@end
