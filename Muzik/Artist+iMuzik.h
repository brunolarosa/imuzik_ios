//
//  Artist+iMuzik.h
//  iMuzik
//
//  Created by Bruno LAROSA on 2/16/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "Artist.h"
#import "GDataXMLNode.h"

@interface Artist (iMuzik)

+(Artist *)artistWithMuzikInfo:(GDataXMLElement *)muzikInfo inManagedObjectContext:(NSManagedObjectContext *)context;

@end
