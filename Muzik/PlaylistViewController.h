//
//  PlaylistViewController.h
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Playlist;

@interface PlaylistViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) Playlist *playlist;

@property (weak, nonatomic) IBOutlet UILabel *playlistNameLabel;
@property (weak, nonatomic) IBOutlet UITableView *tracksTableView;


@end
