//
//  Album+iMuzik.m
//  iMuzik
//
//  Created by Bruno LAROSA on 2/16/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "Album+iMuzik.h"
#import "MuzikFetcher.h"
#import "Song+iMuzik.h"

@implementation Album (iMuzik)

+(Album *)albumWithMuzikInfo:(GDataXMLElement *)muzikInfo inManagedObjectContext:(NSManagedObjectContext *)context {
    Album *album = nil;
    
    NSString *albumId = [[[muzikInfo elementsForName:MUZIK_ALBUM_ID] objectAtIndex:0] stringValue];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Album"];
    request.predicate = [NSPredicate predicateWithFormat:@"unique = %@", albumId];
    NSSortDescriptor *sortDescriptior = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptior];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if(!matches || matches.count > 1) {
        //TODO: Handle Errors
    } else if(matches.count == 0){
        album = [NSEntityDescription insertNewObjectForEntityForName:@"Album" inManagedObjectContext:context];
        album.unique = albumId;
        album.title = [[[muzikInfo elementsForName:MUZIK_ALBUM_TITLE] objectAtIndex:0] stringValue];
        album.imageUrl = [[[muzikInfo elementsForName:MUZIK_ALBUM_IMAGE_URL] objectAtIndex:0] stringValue];
        album.coverData = [NSData dataWithContentsOfURL:[NSURL URLWithString:album.imageUrl]];
        
    } else {
        album = [matches lastObject];
    }
    
    return album;
}

@end
