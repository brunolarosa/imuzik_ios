//
//  AlbumViewController.h
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Album;

@interface AlbumViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) Album *album;

@property (weak, nonatomic) IBOutlet UIImageView *albumCoverImageView;
@property (weak, nonatomic) IBOutlet UITableView *tracksTableView;

@end
