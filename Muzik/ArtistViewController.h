//
//  ArtistViewController.h
//  Muzik
//
//  Created by Bruno LAROSA on 2/17/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Artist;

@interface ArtistViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UILabel *artistNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *artistImageView;
@property (weak, nonatomic) IBOutlet UILabel *artistBioLabel;

@property (weak, nonatomic) IBOutlet UITableView *topTrackTableView;
@property (weak, nonatomic) IBOutlet UICollectionView *albumsCollectionView;


@property (strong, nonatomic) Artist *artist;

@end
