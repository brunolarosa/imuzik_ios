//
//  MenuCell.m
//  iMuzik
//
//  Created by Bruno LAROSA on 2/14/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import "MenuCell.h"

#define TEXT_COLOR [UIColor colorWithRed:169.0/255.0 green:169.0/255.0 blue:169.0/255.0 alpha:1.0]
@implementation MenuCell

@synthesize menuIcon = _menuIcon;
@synthesize menuLabel = _menuLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        self.menuLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 57, 80, 16)];
        [self customMenuLabel];
        [self addSubview:self.menuLabel];
        
        self.menuIcon = [[UIImageView alloc] initWithFrame:CGRectMake(23, 22, 34, 27)];
        [self addSubview:self.menuIcon];
        
        self.selectedBackgroundView.backgroundColor = [UIColor clearColor];
    }
    return self;
}

#pragma mark - Custom UI
- (void)customMenuLabel {
    [self.menuLabel setFont:[UIFont boldSystemFontOfSize:11]];
    [self.menuLabel setTextAlignment:NSTextAlignmentCenter];
    [self.menuLabel setTextColor:TEXT_COLOR];
    [self.menuLabel setBackgroundColor:[UIColor clearColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
