//
//  PlaylistsViewController.h
//  iMuzik
//
//  Created by Bruno LAROSA on 2/12/13.
//  Copyright (c) 2013 Bruno LAROSA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaylistsViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (nonatomic,strong) UIManagedDocument* muzikDatabase;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end
